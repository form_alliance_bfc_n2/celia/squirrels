#' data_act_squirrels
#'
#' A sample of the Squirrels in Central Park
#'
#' @format A data frame with 15 rows and 4 variables:
#' \describe{
#'   \item{ age }{  character }
#'   \item{ primary_fur_color }{  character }
#'   \item{ activity }{  character }
#'   \item{ counts }{  integer }
#' }
#' @source Source
"data_act_squirrels"
