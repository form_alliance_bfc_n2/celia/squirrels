
# <img src="man/figures/squirrels_hex.png" align="right" alt="" width="120" />

<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrels

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrels is to analyse the Squirrels of Central Park. The
package was created in a formation N2.

## Installation

You can install the development version of squirrels like so:

``` r
# install.packages("squirrels")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrels)

get_message_fur_color(primary_fur_color = "Black")
#> We will focus on Black squirrels
check_primary_color_is_ok(string = c(NA, "Black", "Gray"))
#> [1] TRUE
```
